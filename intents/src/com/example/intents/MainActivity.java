package com.example.intents;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends ActionBarActivity {
	private TextView txtview_addback;
	private EditText txtedit_convertnumber;
	private TextView txtview_convertback;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtview_addback = (TextView) this.findViewById(R.id.txtview_addback);
        txtview_convertback = (TextView) this.findViewById(R.id.txtview_convertback);
        txtedit_convertnumber = (EditText) this.findViewById(R.id.txtedit_convertnumber);
    }
    public void openActivity (View view){
    	Intent intent = new Intent(this, Addactivity.class);
    	startActivityForResult(intent,666);
    	
    }
    public void openConvertActivity ( View view){
    	Intent intent = new Intent(this, ConvertActivity.class);
    	intent.putExtra("preconvertValue", txtedit_convertnumber.getText().toString());
    	startActivityForResult(intent,3);
    }
    @Override
    protected void onActivityResult(int requestCode,int
    resultCode,Intent data) {
    	
    	super.onActivityResult(requestCode, resultCode, data);
    	//if (requestCode==666){
    		
    		txtview_addback.setText(data.getStringExtra("extraValue"));
    		txtview_convertback.setText(data.getStringExtra("convertValue"));
    	//}
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
