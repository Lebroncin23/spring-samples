package com.example.intents;

import android.support.v7.app.ActionBarActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.app.AlertDialog;

public class Addactivity extends ActionBarActivity {
	private EditText txtedit_number1;
	private EditText txtedit_number2;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_addactivity);
		txtedit_number1=(EditText) this.findViewById(R.id.txtedit_number1);
		txtedit_number2=(EditText) this.findViewById(R.id.txtedit_number2);
	}
	public void openActivity(View view){
		int result= 0;
		String resultadoFin= "";//si le pasas solo el resultado por int no coge bien el txtview
		
		//showDialog(alerta());
		
		result = Integer.parseInt(txtedit_number1.getText().toString())+
				Integer.parseInt(txtedit_number2.getText().toString());
		resultadoFin= result+"";
		Intent intent = new Intent();
		intent.putExtra("extraValue", resultadoFin);
		setResult(RESULT_OK,intent);
		finish();
	}
	/**
	 * metodo de alerta
	 * @return
	 */
	public AlertDialog alerta(){
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("No has rellenado todos los numeros, deseas continuar?")
		.setCancelable(false)
		.setPositiveButton("Yes", new
			DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					camposVacios();
				}
			})
		.setNegativeButton("No", new
			DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					dialog.cancel();
					//focus en las cajas vacias
				}
			});
		AlertDialog alert = builder.create();
		//why???
		return alert;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.addactivity, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	/**
	 * metodo si los campos estan vacios ponerlos a 0
	 */
	private void camposVacios() {
		if((txtedit_number1.getText().toString().equals(""))||(txtedit_number1.getText().toString().equals(""))){
	
			if((txtedit_number1.getText().toString().equals(""))){
				txtedit_number1.setText("0");
			}
			if((txtedit_number2.getText().toString().equals(""))){
				txtedit_number2.setText("0");
			}
		}
	}
}
