package com.example.intents;

import java.util.ArrayList;
import java.util.List;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

public class ConvertActivity extends ActionBarActivity {
	private TextView txtview_ConvertNumber;
	private Spinner spinner_desplegable;
	private List<String> lista;
	double euros=0.79;
	String nombre="";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_convert);
		txtview_ConvertNumber = (TextView) this.findViewById(R.id.txtview_ConvertNumber);
		spinner_desplegable = (Spinner) this.findViewById(R.id.desplegable);
		txtview_ConvertNumber.setText( getIntent().getStringExtra("preconvertValue"));
		DatosPorDefecto();
	}
	private void DatosPorDefecto() {
		   
		   lista = new ArrayList<String>();
		   lista.add("Cubata");//6
		   lista.add("Chupito");//1.5
		   lista.add("Hamburguesas");//1
		   lista.add("Refresco");//3
		   lista.add("Cerveza");//2
		   lista.add("Barcos y putas");//1000
		   ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, lista);
		   adaptador.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		   spinner_desplegable.setAdapter(adaptador);
		   spinner_desplegable.setOnItemSelectedListener(new OnItemSelectedListener() {
			   @Override
			   public void onItemSelected(AdapterView parent, View view, int position, long id) {
				   selectItem(position, id);
			   }
			   private void selectItem(int position, long id) {
				   switch(position) {
				   case 0: 
				       euros = 6;
				       nombre = "Cubata/s";
				       break;
				   case 1: 
				       euros = 1.5;
				       nombre = "Chupito/s";
				       break;
				   case 2: 
				       euros = 1;
				       nombre = "Hamburguesa/s";
				       break;
				   case 3: 
				       euros = 3;
				       nombre = "Refresco/s";
				       break;
				   case 4:  
				       euros= 2;
				       nombre = "Cerveza/s";
				       break;
				   case 5:
					   euros= 1000;
					   nombre = "Barcos y puta/s";
					   break;
				   default: 
				       euros = 6;
				       nombre = "Cubata/s";
				       break;
				   }

				      
			   }
			   @Override
			   public void onNothingSelected(AdapterView<?> arg0) {
			   }
			});
	}
		   
		   
		
	
	public void openActivity(View view){
		double result= 0;
		
		String resultadoFin= "";//si le pasas solo el resultado por int no coge bien el txtview
		result = Integer.parseInt(txtview_ConvertNumber.getText().toString())/ euros ;
		resultadoFin= getIntent().getStringExtra("preconvertValue")+"� = "+result+ " "+ nombre;
		Intent intent = new Intent();
		intent.putExtra("convertValue", resultadoFin);
		setResult(RESULT_OK,intent);
		finish();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.convert, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
